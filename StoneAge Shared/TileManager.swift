
import SpriteKit
import TiledWorldLogic

class TileManager {
    let tileSet = SKTileSet(named: "Sample Isometric Tile Set")!
    let tileSize = CGSize(width: 128, height: 64)
    let grid: Grid
    
    lazy var tileGroups: [TerrainName: SKTileGroup] = {
        [
            TerrainName.water : tileSet.tileGroups.first { $0.name == TerrainName.water.rawValue }!,
            TerrainName.sand : tileSet.tileGroups.first { $0.name == TerrainName.sand.rawValue }!,
            TerrainName.grass : tileSet.tileGroups.first { $0.name == TerrainName.grass.rawValue }!
        ]
    }()
    
    lazy var mapLayers: [TerrainName: SKTileMapNode] = {
        return [
            TerrainName.water: SKTileMapNode(tileSet: tileSet, columns: grid.cols, rows: grid.rows, tileSize: tileSize),
            TerrainName.sand: SKTileMapNode(),
            TerrainName.grass: SKTileMapNode(),
        ]
    }()
    
    init(grid: Grid) {
        self.grid = grid
    }
}

