
import GameKit
import TiledWorldLogic

struct TerrainType {
    var name: TerrainName
    var maximumAltitude: Int
    var minimumAltitude: Int
}

class TerrainMap {
    let seed: Int
    let grid: Grid
    let amplitude: Float
    var terrainTypes: [TerrainType]?
    private let noiseMap: GKNoiseMap
    
    init(seed: Int, terrainTypes: [TerrainType], grid:Grid, noiseMap: GKNoiseMap, amplitude: Float) {
        self.seed = seed
        self.grid = grid
        self.noiseMap = noiseMap
        self.terrainTypes = terrainTypes
        self.amplitude = amplitude
    }
    
    func terrainType(gridIndex: GridIndex) -> TerrainType? {
        let height = elevation(gridIndex: gridIndex)
        return terrainType(elevation: height)
    }
    
    func terrainType(elevation: Int) -> TerrainType? {
        guard let terrainTypes = terrainTypes else { return nil }
        return terrainTypes.first { (terrainType) -> Bool in
            (terrainType.minimumAltitude...terrainType.maximumAltitude).contains(elevation)
        }
    }

    func elevation(gridIndex: GridIndex) -> Int {
        let offMapHeight = Int.min
        guard grid.gridIndexIsInGrid(gridIndex) else { return offMapHeight }
        let location = vector2(Int32(gridIndex.row), Int32(gridIndex.col))
        let noiseValue = noiseMap.value(at: location)
        return Int(noiseValue*amplitude)
    }
}

