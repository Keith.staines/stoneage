
import GameKit
import TiledWorldLogic

class TerrainGenerator {
    
    func buildTerrain(
        terrainTypes: [TerrainType],
        grid: Grid,
        amplitude: Float,
        seed: Int32 = 474593784) -> TerrainMap {
        let noiseMap = makeNoiseMap(seed: seed, columns: grid.cols, rows: grid.rows)
        return TerrainMap(seed: Int(seed), terrainTypes: terrainTypes, grid: grid, noiseMap: noiseMap, amplitude: amplitude)
    }
    
    private func makeNoiseMap(
        seed: Int32,
        columns: Int,
        rows: Int) -> GKNoiseMap {
        let source = GKPerlinNoiseSource(frequency: 1.0, octaveCount: 6, persistence: 2, lacunarity: 1.5, seed: seed)
        let noise = GKNoise(source)
        let size = vector2(1.0, 1.0)
        let origin = vector2(0.0, 0.0)
        let sampleCount = vector2(Int32(columns), Int32(rows))
        return GKNoiseMap(noise, size: size, origin: origin, sampleCount: sampleCount, seamless: true)
    }
}
