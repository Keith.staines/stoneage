//
//  GameScene.swift
//  StoneAge Shared
//
//  Created by Keith Staines on 10/11/2020.
//

import SpriteKit
import GameKit
import TiledWorldLogic

class GameScene: SKScene {
    
    lazy var tileManager = TileManager(grid: grid)
    let grid = Grid(rows: 128, cols: 128)
    var terrainMap: TerrainMap?
    
    fileprivate lazy var map: SKNode = {
        let map = SKNode()
        map.xScale = 0.05
        map.yScale = 0.05
        for layer in tileManager.mapLayers.values {
            layer.enableAutomapping = true
            map.addChild(layer)
        }
        return map
    }()
    
    func tileMap() {
        terrainMap = buildTerrain()
        for row in 0 ..< grid.rows {
            for col in 0 ..< grid.cols {
                let gridIndex = GridIndex(row: row, col: col)
                guard
                    let terrainMap = terrainMap,
                    let terrainName = terrainMap.terrainType(gridIndex: gridIndex)?.name
                else { continue }
                let tileGroup = tileManager.tileGroups[terrainName]
                let layer = tileManager.mapLayers[terrainName]
                layer?.setTileGroup(tileGroup, forColumn: col, row: row)
            }
        }
    }
    
    func buildTerrain() -> TerrainMap {
        let generator = TerrainGenerator()
        return generator.buildTerrain(
            terrainTypes: terrainTypes,
            grid: grid,
            amplitude: 10)
    }
    
    let terrainTypes = [
        TerrainType(name: .water, maximumAltitude: 0, minimumAltitude: Int.min),
        TerrainType(name: .sand, maximumAltitude: 2, minimumAltitude: 0),
        TerrainType(name: .grass, maximumAltitude: Int.max, minimumAltitude: 2)
    ]
    
    class func newGameScene() -> GameScene {
        guard let scene = SKScene(fileNamed: "GameScene") as? GameScene else {
            print("Failed to load GameScene.sks")
            abort()
        }
        scene.scaleMode = .aspectFill
        return scene
    }
    
    func setUpScene() {
        tileMap()
        addChild(map)
    }
    
    #if os(watchOS)
    override func sceneDidLoad() {
        self.setUpScene()
    }
    #else
    override func didMove(to view: SKView) {
        self.setUpScene()
    }
    #endif
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}

#if os(iOS) || os(tvOS)
// Touch-based event handling
extension GameScene {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let label = self.label {
            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
        }
        
        for t in touches {
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
        }
    }
    
    
}
#endif

#if os(OSX)
// Mouse-based event handling
extension GameScene {
    
    override func mouseDown(with event: NSEvent) {
        
    }
    
    override func mouseDragged(with event: NSEvent) {
    }
    
    override func mouseUp(with event: NSEvent) {
        
    }
    
}
#endif



