
enum TerrainName: String, CaseIterable {
    case water = "Water"
    case sand = "Sand"
    case grass = "Grass"
}
